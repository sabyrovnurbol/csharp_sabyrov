﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    struct sum
    {
        public int a, b;
        public sum(int x, int y)
        {
            a = x;
            b = y;

        }
        public override string ToString()
        {
            return a.ToString() +" "+ b.ToString();
        }


        public static sum operator +(sum arg1, sum arg2)
        {
            arg1.a = arg1.a + arg1.b;
            arg1.b = arg2.a + arg2.b;
            return arg1;
        }
    }
    class Point
    {
        static void Main(string[] args)
        {
            sum x = new sum(1, 2);
            sum y = new sum(2, 3);
            sum kos = x + y;
            Console.WriteLine(kos);
            Console.ReadLine();
        }
    }
        
}
