﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MidTerm
{
    struct Length 
    {
        public int a, b;
        public Length(int x, int y)
        {
            a = x;
            b = y;

        }
       
       public double LineLength(Length arg1, Length arg2)
        {       
           int  x1 =arg1.a - arg1.b;
           int  y1=arg2.a - arg2.b;
           return Math.Sqrt((x1*x1)-(y1*y1));
       
        } 
    }
    class Line
    {
        static void Main(string[] args)
        {
            Length x = new Length (1, 2);
            Length y = new Length (2, 3);
            LineLength(Length x, Length y);
            Console.WriteLine(LineLength());
            Console.ReadLine();
        }
    }
}
