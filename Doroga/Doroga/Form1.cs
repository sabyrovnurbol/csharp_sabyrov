﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Doroga
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        public int z = 0;
        public int zn=1;
        public int qq = 50;
     

        private void timer1_Tick(object sender, EventArgs e)
        {
            Graphics g = this.CreateGraphics();
            Pen p = new Pen (Color.Red, 10);

            if (z % 3==0)
            {
                g.Clear(Color.White);
                g.DrawLine(p, 150, 0, 150, 60);
                g.DrawLine(p, 150, 70, 150, 130);
                g.DrawLine(p, 150, 140, 150, 200);
                g.DrawLine(p, 150, 210, 150, 270);
                g.DrawLine(p, 150, 280, 150, 340);
                g.DrawLine(p, 150, 350, 150, 410);
            }

            if (z %3==1)
            {
                g.Clear(Color.White);
                g.DrawLine(p, 150, 10, 150, 70);
                g.DrawLine(p, 150, 80, 150, 140);
                g.DrawLine(p, 150, 150, 150, 210);
                g.DrawLine(p, 150, 220, 150, 280);
                g.DrawLine(p, 150, 290, 150, 350);
                g.DrawLine(p, 150, 360, 150, 420);
            }

            if (z % 3 == 2)
            {
                g.Clear(Color.White);
                g.DrawLine(p, 150, 20, 150, 80);
                g.DrawLine(p, 150, 90, 150, 150);
                g.DrawLine(p, 150, 160, 150, 220);
                g.DrawLine(p, 150, 230, 150, 290);
                g.DrawLine(p, 150, 300, 150, 360);
                g.DrawLine(p, 150, 370, 150, 430);
            }
            z++;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            Graphics g = this.CreateGraphics();
            Pen k = new Pen(Color.Blue, 5);
             g.DrawEllipse(k, qq, 200, 5, 5);
             qq = qq + 10;
        }
    }

}
