﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FileDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamWriter writer = File.CreateText(@"C:\newfile.txt"); 
            writer.WriteLine("This is my new file");
            writer.WriteLine("Do u like its format?");
            writer.Close();



            StreamReader reader = File.OpenText(@"C:\newfile.txt");
            string contents = reader.ReadToEnd();
            reader.Close();
            Console.WriteLine(contents);
            Console.ReadLine();

        }
    }
}
