﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace FileWatchingDemo
{
    class Program
    {
        static void Main(string[] args)
        {
    FileSystemWatcher watcher =new FileSystemWatcher(Environment.SystemDirectory);
    watcher.Path = "c:/";
    watcher.Filter = "*.ini";
    watcher.IncludeSubdirectories = true;
    watcher.NotifyFilter =NotifyFilters.Attributes | NotifyFilters.Size;
    watcher.Changed +=new FileSystemEventHandler(watcher_Changed);
    watcher.EnableRaisingEvents = true;
    Console.ReadKey();
        }

        static void watcher_Changed(object sender,FileSystemEventArgs e)
        {
            Console.WriteLine("Changed: {0}", e.FullPath);
        }

    }
}
