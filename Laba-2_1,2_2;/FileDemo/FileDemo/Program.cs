﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FileDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamWriter writer = File.CreateText(@"C:\who is the champion.txt"); 
            writer.WriteLine("Real Madrid is the CHAMPION");
            writer.WriteLine("Do YOU agree?, Yes, You do!!!");
            writer.Close();



            StreamReader reader = File.OpenText(@"C:\who is the champion.txt");
            string answer = reader.ReadToEnd();
            reader.Close();
            Console.WriteLine(answer);
            Console.ReadLine();

        }
    }
}
