﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigNumber
{
    class Program
    {
        static void Main(string[] args)
        {
            string s1 = Console.ReadLine();
            string s2 = Console.ReadLine();
            int[] mas1 = new int[1000];
            int[] mas2 = new int[1000];
            int[] sum = new int[1000];
            int ms1 = 0;
            int ms2 = 0;
            for (int i = (s1.Length - 1); i >= 0; i--, ms1++)
            {   mas1[ms1] = s1[i] - '0';}


            for (int i = (s2.Length - 1); i >= 0; i--, ms2++)
            {   mas2[ms2] = s2[i] - '0';}

            int max = Math.Max(s1.Length, s2.Length);

            for (int i = 0; i < 1000; i++)
            {   sum[i] = 0;}

            for (int i = 0; i <max; i++)
            {
                sum[i] = sum[i] + (mas1[i] + mas2[i]) % 10;
                sum[i + 1] = (mas1[i] + mas2[i]) / 10;
            }

            bool poscifra= false;
            if (sum[max] == 1) poscifra = true;
            if (poscifra==true)
                            {
                                for (int i = max; i >= 0; i--)
                                {
                                    Console.Write(sum[i]);
                                }
                            }
            else
            {
                for (int i = max- 1; i >= 0; i--)
                {
                    Console.Write(sum[i]);
                }
            }
            Console.ReadKey();
        }
    }
}
