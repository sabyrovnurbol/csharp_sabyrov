﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DynamicINTERFACE
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

        }
        int x = 50;
        int y = 50;
        int z = 0;
        int number;
        int number2;
        int k = 0;
        int result=0;
        double result2 = 0;
        private void Form1_Load(object sender, EventArgs e)
        {
            int i;
            int j;

            for (i = 0; i <= 2; i++)
            {
                for (j = 1; j <= 3; j++)
                {
                    z = i * 3 + j;
                    Button a = new Button();
                    a.Text = z.ToString();
                    a.BackColor = Color.White;
                    a.Size = new Size(50, 50);
                    a.Location = new Point(y * j, (i + 1) * x);
                    this.Controls.Add(a);
                    a.Click += a_Click;
                }
            }
            Button o = new Button();
            o.Text = "0";
            o.BackColor = Color.White;
            o.Size = new Size(50, 50);
            o.Location = new Point(100, 200);
            this.Controls.Add(o);
            o.Click += o_Click;

            Button c = new Button();
            c.Text = "C";
            c.BackColor = Color.White;
            c.Size = new Size(50, 50);
            c.Location = new Point(275,50 );
            this.Controls.Add(c);
            c.Click += c_Click;

            Button ce = new Button();
            ce.Text = "CE";
            ce.BackColor = Color.White;
            ce.Size = new Size(50, 50);
            ce.Location = new Point(275, 100);
            this.Controls.Add(ce);
            ce.Click += ce_Click;

            Button p = new Button();
            p.Text = "+";
            p.BackColor = Color.White;
            p.Size = new Size(50, 50);
            p.Location = new Point(225, 50);
            this.Controls.Add(p);
            p.Click += p_Click;

            Button m = new Button();
            m.Text = "-";
            m.BackColor = Color.White;
            m.Size = new Size(50, 50);
            m.Location = new Point(225, 100);
            this.Controls.Add(m);
            m.Click += m_Click;

            Button um = new Button();
            um.Text = "*";
            um.BackColor = Color.White;
            um.Size = new Size(50, 50);
            um.Location = new Point(225, 150);
            this.Controls.Add(um);
            um.Click += um_Click;

            Button de = new Button();
            de.Text = "/";
            de.BackColor = Color.White;
            de.Size = new Size(50, 50);
            de.Location = new Point(225, 200);
            this.Controls.Add(de);
            de.Click += de_Click;

            Button r= new Button();
            r.Text = "=";
            r.BackColor = Color.White;
            r.Size = new Size(50, 100);
            r.Location = new Point(275, 150);
            this.Controls.Add(r);
            r.Click += r_Click;
          
        }
        private void a_Click(object sender, EventArgs e)
        {

            Button TheButtonClicked = sender as Button;
            textBox1.Text = textBox1.Text + TheButtonClicked.Text;
        }

        private void o_Click(object sender, EventArgs e)
        {

            Button TheButtonClicked = sender as Button;
            textBox1.Text = textBox1.Text + TheButtonClicked.Text;
        }

        private void c_Click(object sender, EventArgs e)
        {

            Button TheButtonClicked = sender as Button;
            textBox1.Text = textBox1.Text.Substring(0, textBox1.Text.Length - 1);
        }
        private void ce_Click(object sender, EventArgs e)
        {

            Button TheButtonClicked = sender as Button;
            textBox1.Text = null;
        }

        private void p_Click(object sender, EventArgs e)
        {

            Button TheButtonClicked = sender as Button;
            number = int.Parse(textBox1.Text);
            textBox1.Text = null;
            k = 1;
         
        }
        private void m_Click(object sender, EventArgs e)
        {

            Button TheButtonClicked = sender as Button;
            number = int.Parse(textBox1.Text);
            textBox1.Text = null;
            k = 2;
        }   
        private void de_Click(object sender, EventArgs e)
        {

            Button TheButtonClicked = sender as Button;
            number = int.Parse(textBox1.Text);
            textBox1.Text = null;
            k = 3;
        }
        private void um_Click(object sender, EventArgs e)
        {

            Button TheButtonClicked = sender as Button;
            number = int.Parse(textBox1.Text);
            textBox1.Text = null;
            k = 4;
        }

        private void r_Click(object sender, EventArgs e)
        {

            Button TheButtonClicked = sender as Button;
            number2 = int.Parse(textBox1.Text);
            if (k == 1) { result = number + number2; textBox1.Text = Convert.ToString(result); }
            if (k == 2) { result = number - number2; textBox1.Text = Convert.ToString(result);  }
            if (k == 3) { result2 = number / number2; textBox1.Text = Convert.ToString(result2); }
            if (k == 4) { result = number * number2; textBox1.Text = Convert.ToString(result); }
          
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}

