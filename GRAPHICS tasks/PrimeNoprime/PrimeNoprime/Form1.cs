﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PrimeNoprime
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            Graphics g = this.CreateGraphics();
            g.Clear(Color.White);
            int x = int.Parse(textBox1.Text);
            int count = 0;
            for (int i = 2; i < x; i++)
            {
                if (x% i == 0)       
                {
                    count++;
                }
            }
            if (count == 0) { label1.Text = "Prime number"; }
            if (count !=0 )  { label1.Text = "Not prime number"; } 

        }
    }
}
