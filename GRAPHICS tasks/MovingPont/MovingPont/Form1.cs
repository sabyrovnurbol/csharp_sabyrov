﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MovingPont
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        Graphics g;
        Pen p = new Pen(Color.Red, 10);
        int x=100;
        int y=100;
        int a = 0;
        int b = 0;
        
        private void Form1_Paint(object sender, PaintEventArgs e)
        {
              
        }

        private void Form1_Load(object sender, EventArgs e)
        {
                  
        }

        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
          
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Right) { y = 0; x = 10; }
            if (e.KeyCode ==Keys.Left) { y = 0; x = -10; }
            if (e.KeyCode ==Keys.Up) { y = -10; x = 0; }
            if (e.KeyCode == Keys.Down) { y = 10; x = 0; }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {

            g = this.CreateGraphics();
            g.Clear(Color.White);
            a += x;
            b += y;
            g.DrawEllipse(p, a, b, 50, 50);   

 
        }

        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {

        }
    }
}
