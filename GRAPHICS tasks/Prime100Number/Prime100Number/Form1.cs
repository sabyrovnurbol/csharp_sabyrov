﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prime100Number
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }

        int p = 0;
        int k = 0;
        int[] mass = new int[100];

        private void timer1_Tick(object sender, EventArgs e)
        {
            label1.Text+=mass[p].ToString() + " ";
            p++;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            for (int i = 2; i <= 100; i++)
            {
                int count = 0;
                for (int j = 1; j <= i; j++) { if (i % j == 0)  count++; }
                if (count == 2) { mass[k] = i; k++; }
            }
        }
    }
}
