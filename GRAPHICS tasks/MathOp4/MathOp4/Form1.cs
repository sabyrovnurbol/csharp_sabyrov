﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MathOp4
{
    public partial class Form1 : Form
    {


        public Form1()
        {
            InitializeComponent();
            textBox1.ForeColor = Color.Gray;
            textBox1.Text = "Enter number here";
            this.textBox1.Leave += new System.EventHandler(this.textBox1_Leave);
            this.textBox1.Enter += new System.EventHandler(this.textBox1_Enter);
            textBox2.ForeColor = Color.Gray;
            textBox2.Text = "Enter number here";
            this.textBox2.Leave += new System.EventHandler(this.textBox2_Leave);
            this.textBox2.Enter += new System.EventHandler(this.textBox2_Enter);
        }

        private void textBox1_Leave(object sender, EventArgs e)
        {
            if (textBox1.Text.Length == 0)
            {
                textBox1.Text = "Enter number here";
                textBox1.ForeColor = Color.Gray;
                textBox1.Font = new Font("Times New Roman", 8.0f, FontStyle.Bold | FontStyle.Italic );
                 }
        }

        private void textBox1_Enter(object sender, EventArgs e)
        {
            if (textBox1.Text == "Enter number here")
            {
                textBox1.Text = "";
                textBox1.ForeColor = Color.Blue;
            }
        }

        private void textBox2_Leave(object sender, EventArgs e)
        {
            if (textBox2.Text.Length == 0)
            {
                textBox2.Text = "Enter number here";
                textBox2.ForeColor = Color.Gray;
                textBox2.Font = new Font("Times New Roman", 8.0f, FontStyle.Bold | FontStyle.Italic);
            }
        }

        private void textBox2_Enter(object sender, EventArgs e)
        {
            if (textBox2.Text == "Enter number here")
            {
                textBox2.Text = "";
                textBox2.ForeColor = Color.Blue;
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            Graphics g = this.CreateGraphics();
            g.Clear(Color.White);
            int x = int.Parse(textBox2.Text);
            int y = int.Parse(textBox1.Text);
            int r = x + y;
            label1.Text = Convert.ToString(r) ; 
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            System.Windows.Forms.ToolTip ToolTip1 = new System.Windows.Forms.ToolTip();
            ToolTip1.SetToolTip(this.textBox2, "Write the first number");
            ToolTip1.SetToolTip(this.textBox1, "Write the second number");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Graphics g = this.CreateGraphics();
            g.Clear(Color.White);
            int x = int.Parse(textBox2.Text);
            int y = int.Parse(textBox1.Text);
            int r = x-y;
            label1.Text = Convert.ToString(r); 
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Graphics g = this.CreateGraphics();
            g.Clear(Color.White);
            int x = int.Parse(textBox2.Text);
            int y = int.Parse(textBox1.Text);
            int r = x * y;
            label1.Text = Convert.ToString(r); 
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Graphics g = this.CreateGraphics();
            g.Clear(Color.White);
            double x = int.Parse(textBox2.Text);
            double y = int.Parse(textBox1.Text);
            double r = x / y;
            label1.Text = r.ToString();
        }
    }
}
